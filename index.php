<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S01: PHP Basics and Selection Control Structures</title>
	</head>
	<body>
		<h1>Echoing Values</h1>

		<!-- USING VARIABLES in echoing strings -->
		<p><?php echo "Good day $name! Your given email is $email. Your age is $age."; ?></p>
		<!-- Use double quotes when echoing strings to recognize 'literals'. Also end with semicolons for good practice. -->

		<!-- USING CONSTANTS -->
		<p><?php echo "The value of Pi is " . PI . "."; ?></p>
		<!-- Dollar sign($) is no longer necessary for constant 'literals'. -->

		<!-- USING BOOLEAN -->
		<p><?php echo $hasTravelledAbroad ?></p>
		<p><?php echo $haveSymptoms ?></p>

		<p><?php echo gettype($hasTravelledAbroad) ?></p>
		<!-- Use gettype to display data type -->

		<p><?php echo var_dump($haveSymptoms) ?></p>
		<!-- Use var_dump to display data type and value -->

		<p><?php echo $spouse ?></p>
		<!-- Result is empty because of null data type/value -->

		<p><?php echo var_dump($spouse) ?></p>
		<!-- Results in NULL being displayed -->

		<!-- USING ARRAYS -->
		<p><?php echo $grades[3] ?></p>
		<p><?php echo $grades[1] ?></p>
		<!-- Target specific indices when echoing arrays -->

		<!-- USING OBJECTS -->
		<p><?php echo $gradesObj->firstGrading ?></p>
		<p><?php echo $personObj->address->state ?></p>
		<!-- Use '->' to access a property of the object (works like dot notation) -->

		<h1>Operators</h1>
		<p>X: <?php echo $x ?></p>
		<p>Y: <?php echo $y ?></p>

		<h2>Arithmetic Operators</h2>
		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>

		<h2>Equality Operators</h2>
		<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
		<p>Strict Equality: <?php echo var_dump($x === '1342.14'); ?></p>
		<p>Loose Equality: <?php echo var_dump($x != '1342.14'); ?></p>
		<p>Strict Equality: <?php echo var_dump($x !== '1342.14'); ?></p>
		<!-- Strict equality also compares data types -->

		<h2>Greater/Lesser Operators</h2>
		<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
		<p>Is Greater: <?php echo var_dump($x > $y); ?></p>
		<!-- <=, >=, for less than/equal, greater than/equal -->

		<h2>Logical Operators</h2>
		<p>Are All Requirements Met?: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
		<p>Are Some Requirements Met?: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
		<p>Are Some Requirements Met?: <?php echo var_dump($isLegalAge && !$isRegistered); ?></p>

		<h2>Function</h2>
		<p>Full Name: <?php echo getFullName('John', 'D.', 'Smith'); ?></p>

		<h2>If-elseif-else Statement</h2>
		<p><?php echo determineTyphoonIntensity(12) ?></p>

		<h2>Switch Statements</h2>
		<p><?php echo determineComputerUser(5) ?></p>

		<h2>Ternary Operator Sample</h2>
		<p>78: <?php var_dump(isUnderAge(78)); ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php echo greeting(12) ?></p>

	</body>
</html>