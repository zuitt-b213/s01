<?php

// Single Line comments [ctrl + /]
/*
	Multiline comments [ctrl + shft + /]
*/


// Variables
	// in PHP are defined using dollar ($) notation before the name of the variable. ex: $varName;

$name = 'John Smith'; // php often uses single quotes to declare strings and ALWAYS end with semicolon
$email = 'johnsmith@mail.com';

// Constants
	// in PHP are defined using the define() function
	// Syntax: define(name, value)

define('PI', 3.1416);

// DATA TYPES

//  Strings
$state = 'New York';
$country = 'United States of America';

// Integers
$age = 31;
$headcount = 27;

// Floating points
$grade = 98.2;
$distanceInKilometers = 526.28;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Null
$spouse = null;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

// Objects
	/*
		Syntax:
			$objectName = (object)[
				'key1' => value1,
				'key2' => value2
			];
	*/
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'USA'
	]
];

// OPERATORS

// Assignment operators - used to assign values to variables (=)
$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// FUNCTIONS
function getFullName($firstName, $middleInitial, $lastName) {
	return "$lastName, $firstName $middleInitial";
}

// SELECTION CONTROL STRUCTURES

// If-elseif-else Statements
function determineTyphoonIntensity($windSpeed) {
	if($windSpeed < 30) {
		return 'Not a typhoon yet';
	}
	else if($windSpeed <= 61) {
		return 'Tropical depression detected';
	}
	else if($windSpeed >= 62 && $windSpeed <= 88) {
		return 'Tropical storm detected';
	}
	else if($windSpeed >= 89 && $windSpeed <= 117) {
		return 'Tropical storm detected';
	}
	else{
		return 'Typhoon detected.';
	}
}

// Switch Statements
function determineComputerUser($computerNumber){
	switch($computerNumber){
		case 1:
			return 'Linus Trovalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Onel De Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber . ' is out of bounds.';
	}
}

// Ternary Operator
function isUnderAge($age){
	return ($age < 18) ? true : false;
}

// Try-Catch-Finally
function greeting($str){
	try{
		// This will attempt to execute a code
		if(gettype($str) == "string"){
			echo $str;
		}
		else{
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}
	finally{
		// This will continue execution of code regardless of success or failure of 'try' block.
		echo " I did it again!";
	}
}

?>